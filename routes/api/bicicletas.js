var express = require('express');
var router = express.Router();
var bicicletaController = require('../../controllers/api/bicicletaApiController');

router.get('/', bicicletaController.bicicleta_list);

router.post('/create', bicicletaController.bicicleta_add);

router.delete('/delete', bicicletaController.bicicleta_delete);

router.patch('/update', bicicletaController.bicicleta_update);

module.exports = router;