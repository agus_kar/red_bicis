var mymap = L.map('main_map').setView([-38.0174106, -57.6705735], 12);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?{foo}', {foo: 'bar', attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'}).addTo(mymap);

var marker = L.marker([-38.0174106, -57.6705735],{title: "Mar del Plata"}).addTo(mymap);

$.ajax({
    dataType: 'json',
    url: 'api/bicicletas',
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.modelo}).addTo(mymap);
        })
    }
})