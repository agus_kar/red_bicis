var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicletas');

describe('Testing Bicicletas', function(){
    beforeEach(function(done){

        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, "MongoDB connection error"));
        db.on('open', function(){
            console.log('We are connected to test database');
            done();
        });
    });
    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('Crea una instancia de Bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34, -54]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(-34);
            expect(bici.ubicacion[1]).toEqual(-54);
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('Comienza vacia', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('Agrega solo una bici', (done) => {
            var aBici = new Bicicleta({code: 1, color: "marron", modelo: "urbana"});
            Bicicleta.add(aBici, function(err, bicis){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    
                    done();
                })
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('Busco una bici', (done) => {
            // Bicicleta.allBicis(function(err, bicis){
                // expect(bicis.length).toBe(0);

                var bici = new Bicicleta({code: 1, color: 'rojo', modelo: 'urbana'});
                Bicicleta.add(bici, function(err, newBici){
                    if (err) console.log(err);

                    
                    var bici2 = new Bicicleta({code: 2, color: 'verde', modelo: 'mtb'});
                    
                    Bicicleta.add(bici2, function(err, newBici){
                        if (err) console.log(err);
                        
                        Bicicleta.findByCode(1, function(error, targetBici){
                            expect(targetBici.code).toBe(bici.code);
                            done();
                        });
                    });
                });
            // });
        });
    });

});

// beforeEach(()=>{
//     Bicicleta.allBicis = [];
// });

// describe('Bicicleta.allBicis', () => {
//     it('comienza vacio', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//     });
// });

// describe('Bicicleta.add', () => {
//     it('agregamos una bici', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);

//         var a = new Bicicleta(2, 'rojo', 'montaña', [-38.0019066, -57.5810998]);
//         Bicicleta.add(a);
//         expect(Bicicleta.allBicis.length).toBe(1);
//         expect(Bicicleta.allBicis[0]).toBe(a);

//     });
// });


// describe('Bicicleta.findById', () => {
//     it('buscamos por ID', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
        
//         var b = new Bicicleta(2, 'rojo', 'montaña', [-38.0019066, -57.5810998]);
//         var c = new Bicicleta(3, 'verde', 'playera', [-38.0019066, -57.5810998]);
//         Bicicleta.add(b);
//         Bicicleta.add(c);

//         var biciEncontrada = Bicicleta.findById(2);
//         expect(biciEncontrada.id).toBe(2);
//         expect(biciEncontrada.color).toBe(b.color);
//     });
// });

// describe("Bicicleta.removeById", () => {
//     it('borramos por ID', () => {
        
//         expect(Bicicleta.allBicis.length).toBe(0);
        
//         var d = new Bicicleta(2, 'rojo', 'montaña', [-38.0019066, -57.5810998]);
//         var e = new Bicicleta(3, 'verde', 'playera', [-38.0019066, -57.5810998]);
//         Bicicleta.add(d);
//         Bicicleta.add(e);
//         expect(Bicicleta.allBicis.length).toBe(2);
        
//         Bicicleta.removeById(3);
//         expect(Bicicleta.allBicis.length).toBe(1);
//     });
// });